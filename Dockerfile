FROM python:3.10.7 as base

WORKDIR app
RUN apt-get update
RUN pip install poetry
RUN poetry config virtualenvs.create false

FROM base

COPY pyproject.toml ./
RUN poetry install --only main

COPY . ./
RUN python manage.py collectstatic

ENTRYPOINT ["uwsgi", "--http" ,":8000", "--module", "mvlmix.wsgi:application"]

