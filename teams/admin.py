from django.contrib import admin

# Register your models here.
from teams.models import DivisionModel, PlayerModel, RoleModel, TeamModel

# Register your models here.


class PlayerAdminModel(admin.ModelAdmin):
    list_display = [
        "lastname",
        "firstname",
        "team_name",
        "division_name",
        "is_approved",
    ]



admin.site.register(PlayerModel, PlayerAdminModel)
admin.site.register(TeamModel)
admin.site.register(DivisionModel)
admin.site.register(RoleModel)
