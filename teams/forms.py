from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator

from teams.models import DivisionModel, PlayerModel, RoleModel, TeamModel


class PlayerForm(forms.Form):
    lastname = forms.CharField(max_length=50, min_length=1, label="Фамилия")
    firstname = forms.CharField(max_length=50, min_length=1, label="Имя")
    role = forms.ModelChoiceField(queryset=RoleModel.objects.all(), label="Амплуа")
    growth = forms.IntegerField(
        validators=[MinValueValidator(100), MaxValueValidator(300)], label="Рост"
    )
    year_of_birth = forms.IntegerField(
        validators=[MinValueValidator(1900), MaxValueValidator(2100)],
        label="Год рождения",
    )
    team_name = forms.ModelChoiceField(
        queryset=TeamModel.objects.order_by("id").all(),
        label="Введите название команды",
    )
    division_name = forms.ModelChoiceField(
        queryset=DivisionModel.objects.order_by("id").all(), label="Дивизион"
    )
    photo = forms.ImageField(label="Фотография")


class TeamForm(forms.Form):
    name = forms.CharField(max_length=50, min_length=1, label="Название команды")
    division_name = forms.ModelChoiceField(
        queryset=DivisionModel.objects.all(), label="Дивизион"
    )
