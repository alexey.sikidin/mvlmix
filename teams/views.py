from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.views import LoginView as BaseLoginView
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic

from teams.forms import PlayerForm, TeamForm
from teams.models import PlayerModel, TeamModel


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("login")


class HomeView(generic.TemplateView):
    template_name = "teams/home.html"


class AllTeamsView(generic.ListView):
    model = TeamModel
    template_name = "teams/all-teams.html"
    context_object_name = "teams"

    def get_queryset(self):
        return TeamModel.get_approved()


class OneDivisionView(generic.ListView):
    model = TeamModel
    template_name = "teams/all-teams.html"
    context_object_name = "teams"

    def get_queryset(self):
        return (
            TeamModel.get_approved()
            .filter(division_name=self.kwargs["id"])
            .order_by("id")
        )


class OneTeamView(generic.ListView):

    model = PlayerModel
    template_name = "teams/all-players-one-team.html"
    context_object_name = "players"

    def get_queryset(self):
        return PlayerModel.get_approved().filter(team_name=self.kwargs["pk"]).order_by("id")


class CreateTeamView(LoginRequiredMixin, PermissionRequiredMixin, generic.FormView):
    template_name = "teams/create-team.html"
    form_class = TeamForm
    permission_required = "teams.add_teammodel"
    success_url = "/teams"

    def form_valid(self, form):
        TeamModel(
            name=form.data.get("name"), division_name_id=form.data.get("division_name")
        ).save()
        return HttpResponseRedirect("/teams")


class AllPlayersView(PermissionRequiredMixin, generic.ListView):
    model = PlayerModel
    template_name = "teams/all-players.html"
    permission_required = "teams.view_playermodel"
    context_object_name = "players"

    def get_queryset(self):
        query_set = PlayerModel.get_approved().order_by("is_approved", "id").all()
        if filter := self.request.GET.get("filter"):
            query_set = query_set.filter(lastname__contains=filter)
        return query_set


class OnePlayerView(generic.DetailView):
    model = PlayerModel
    template_name = "teams/one-player.html"
    context_object_name = "player"


class CreatePlayerView(PermissionRequiredMixin, LoginRequiredMixin, generic.FormView):
    template_name = "teams/create-player.html"
    form_class = PlayerForm
    permission_required = "teams.add_playermodel"
    success_url = "/players"

    def form_valid(self, form):
        PlayerModel(
            firstname=form.data.get("firstname"),
            lastname=form.data.get("lastname"),
            growth=form.data.get("growth"),
            year_of_birth=form.data.get("year_of_birth"),
            role_id=form.data.get("role"),
            team_name_id=form.data.get("team_name"),
            division_name_id=form.data.get("division_name"),
            photo=form.cleaned_data.get("photo"),
        ).save()
        return HttpResponseRedirect("/players")

    def get_permission_denied_message(self):
        permission_denied_message = "Недостаночно прав"
        return permission_denied_message


class RegisterUserView(generic.CreateView):
    template_name = "teams/register.html"
    form_class = UserCreationForm
    success_url = reverse_lazy("home")


class LoginView(BaseLoginView):
    template_name = "teams/login.html"

    def get_default_redirect_url(self):
        if self.next_page:
            return reverse(self.next_page)
        else:
            return reverse("home")
