from django.urls import path

from teams import views

urlpatterns = [
    path("", views.HomeView.as_view(), name="home"),
    path("teams", views.AllTeamsView.as_view(), name="all-teams"),
    path("teams/<int:pk>", views.OneTeamView.as_view(), name="one-team"),
    path("team/create", views.CreateTeamView.as_view(), name="create-team"),
    path("player/create", views.CreatePlayerView.as_view(), name="create-player"),
    path("players", views.AllPlayersView.as_view(), name="all-players"),
    path("player/<int:pk>", views.OnePlayerView.as_view(), name="one-player"),
    path("register", views.RegisterUserView.as_view(), name="register"),
    path("login", views.LoginView.as_view(), name="login"),
    path("logout", views.logout_view, name="logout"),
    path(
        "teams/division/<int:id>", views.OneDivisionView.as_view(), name="one-division"
    ),
]
