from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class PlayerModel(models.Model):
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    growth = models.IntegerField(
        validators=[MinValueValidator(100), MaxValueValidator(300)]
    )
    year_of_birth = models.IntegerField(
        validators=[MinValueValidator(1900), MaxValueValidator(2100)]
    )
    role = models.ForeignKey("RoleModel", on_delete=models.DO_NOTHING)
    team_name = models.ForeignKey("TeamModel", on_delete=models.DO_NOTHING)
    division_name = models.ForeignKey("DivisionModel", on_delete=models.DO_NOTHING)
    photo = models.ImageField(upload_to="images", default=None, null=True)
    is_approved = models.BooleanField(default=False)
    date_create = models.DateTimeField(auto_now_add=True, null=True)
    date_change = models.DateTimeField(auto_now=True)

    def __str__(self):
        return (
            f"{self.id} - {self.lastname} {self.firstname}, "
            f"команда - {self.team_name.name}, дивизион - {self.division_name.name}"
        )

    @classmethod
    def get_approved(cls):
        return cls.objects

    class Meta:
        verbose_name = "Игроки"
        verbose_name_plural = "Игроки"


class TeamModel(models.Model):

    name = models.CharField(max_length=50)
    division_name = models.ForeignKey(
        "DivisionModel", on_delete=models.SET_NULL, null=True
    )
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.id} - {self.name}"

    @classmethod
    def get_approved(cls):
        return cls.objects.filter(is_approved=True).order_by("id")

    class Meta:
        verbose_name = "Команды"
        verbose_name_plural = "Команды"


class DivisionModel(models.Model):

    name = models.CharField(max_length=3)

    def __str__(self):
        return f"{self.id} - {self.name}"

    class Meta:
        verbose_name = "Дивизионы"
        verbose_name_plural = "Дивизионы"


class RoleModel(models.Model):

    name = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.id} - {self.name}"

    class Meta:
        verbose_name = "Роли"
        verbose_name_plural = "Роли"